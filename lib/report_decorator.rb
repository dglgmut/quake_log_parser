require 'json'

#This class is responsible for extending the GameStateMapper
#it prints it's data in JSON, ready to be thrown at a file
class ReportDecorator
  def initialize(game_state_mapper)
    @game_state_mapper = game_state_mapper
  end

  def details_for_every_game_in_json
    every_game_report = @game_state_mapper.games_names.map do |game_name|
      details_for(game_name)
    end
    every_game_report << top_rank
    "[#{every_game_report.join(",\n")}]"
  end

  def details_for(game_name)
    hash = {
      game_name => {
        total_kills: @game_state_mapper.count_kills_at(game_name),
        players: @game_state_mapper.group_players_at(game_name),
        kills: @game_state_mapper.group_players_and_kills_at(game_name),
        death_causes: @game_state_mapper.ordered_counting_of_death_causes_at(game_name)
      }
    }

    JSON.pretty_generate(hash)
  end

  def top_rank
    JSON.pretty_generate({ranking: @game_state_mapper.players_ordered_by_kill_count})
  end
end
