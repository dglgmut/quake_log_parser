require 'singleton'

class GameState < Array
  include Singleton

  def self.write(game_name, killer_name, killed_name, death_cause)
    raise ArgumentError.new("game_name cannot be blank") if game_name.empty?
    instance << [game_name, killer_name, killed_name, death_cause]
  end

  def self.read
    instance
  end
end
