require 'game_state'

#This class is responsible for saving each line from a file at the GameState
class GameStateWriter
  def initialize(log_file_path)
    @file = File.open(log_file_path).read
    @current_game_name = ""
  end

  def load
    @file.each_line do |line|
      case line
      when /.*InitGame.*/
        generate_current_game_name
      when /.*Kill.*/
        insert_game_state(line)
      else
        #ignore
      end
    end
  end

  private
    # Creates a random name identifier so the game can be sorted out later
    def generate_current_game_name
      @current_game_name = "game-#{rand(10**10)}"
    end

    def insert_game_state(line)
      killer_name = line[/: [\w\<\>\s]+killed/][2..-8]
      killed_name = line[/killed\s[\w\s]+by MOD_/][7..-9]
      death_cause = line[/MOD_\S+/]
      GameState.write(@current_game_name, killer_name, killed_name, death_cause)
    end
end
