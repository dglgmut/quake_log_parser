require 'game_state_writer'
require 'game_state_mapper'
require 'report_decorator'

class QuakeLogParser
  def self.print_to_a_report(log_file_path)
    GameStateWriter.new(log_file_path).load
    puts ReportDecorator.new(GameStateMapper.new(GameState.read)).details_for_every_game_in_json
  end
end
