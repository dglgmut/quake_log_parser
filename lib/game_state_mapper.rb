#This class is responsible for exposing the data stored at the GameState
class GameStateMapper
  def initialize(current_game_state)
    @current_game_state = current_game_state
  end

  def games_names
    @current_game_state.map {|record| record[0]}.uniq
  end

  def killers_at(game_name)
    select_game(game_name).map {|record| record[1]}
  end

  def players_killed_by_the_world_at(game_name)
    select_game(game_name).select {|record| record[1] == "<world>" }.map {|record| record[2]}
  end

  def death_causes_at(game_name)
    select_game(game_name).map {|record| record[3]}
  end

  def count_kills_at(game_name)
    select_game(game_name).length
  end

  def group_players_at(game_name)
    select_game(game_name).map {|record| [record[1], record[2]]}.flatten.uniq - ["<world>"]
  end

  def group_players_and_kills_at(game_name)
    players_at_the_game_hash = group_players_at(game_name).inject(Hash.new(0)) { |hash, e| hash[e] = 0 ;hash}

    select_game(game_name).map {|record| [record[1], record[2]]}.inject(players_at_the_game_hash) do |hash, e|
      if e[0] == "<world>"
        hash[e[1]] -= 1
      else
        hash[e[0]] += 1
      end
      hash
    end.sort_by {|_, value| -value}.to_h
  end

	def ordered_counting_of_death_causes_at(game_name)
    unordered_death_count = select_game(game_name).inject(Hash.new(0)) do |hash, e|
      hash[e[3]] += 1
      hash
    end

    unordered_death_count.sort_by {|_, value| -value}.to_h
  end

  def players_ordered_by_kill_count
    games_score_array = games_names.map do |game_name|
      group_players_and_kills_at(game_name)
    end

    #summing up the scores of every game
    scores_summed_by_player = games_score_array.inject{|hash, e| hash.merge( e ){|k, old_v, new_v| old_v + new_v}}

    scores_summed_by_player.sort_by {|_, value| -value}.to_h
  end

  private
    def select_game(game_name)
      @current_game_state.select {|record| record[0] == game_name}
    end
end
