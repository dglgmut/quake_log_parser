Gem::Specification.new do |s|
  s.name = %q{quake_log_parser}
  s.authors = ["Renan Florez"]
  s.version = "1.0.0"
  s.date = %q{2019-07-05}
  s.summary = %q{A parser for Quake's log}
  s.required_ruby_version     = ">= 2.3.0"
  s.required_rubygems_version = ">= 2.5.0"
  s.files = Dir.glob("{bin,lib}/**/*", File::FNM_DOTMATCH).reject {|f| File.directory?(f) }
  s.executables << 'quake_log_parser'
  s.require_paths = ["lib"]
end
