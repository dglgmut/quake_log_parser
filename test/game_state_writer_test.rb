require 'minitest/autorun'
require 'game_state_writer'
require 'tempfile'

class GameStateWriterTest < Minitest::Test
  def setup
    @single_game_file = Tempfile.new('single_game_file')
    @single_game_file << "20:37 InitGame\n"
    @single_game_file << "20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT\n"
    @single_game_file << "1:26 Kill: 1022 4 22: <world> killed Zeh by MOD_TRIGGER_HURT\n"
    @single_game_file << "2:37 Kill: 3 2 10: Isgalamido killed Dono da Bola by MOD_RAILGUN\n"
    @single_game_file.rewind

    @multiple_games_file = Tempfile.new('multiple_games_file')
    @multiple_games_file << "20:37 InitGame\n"
    @multiple_games_file << "20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT\n"
    @multiple_games_file << "20:56 InitGame\n"
    @multiple_games_file << "1:26 Kill: 1022 4 22: <world> killed Zeh by MOD_TRIGGER_HURT\n"
    @multiple_games_file << "2:37 Kill: 3 2 10: Assasinu Credi killed Dono da Bola by MOD_ROCKET_SPLASH\n"
    @multiple_games_file.rewind

    GameState.instance.clear
  end

  def test_each_death_is_stored_at_game_state
    GameStateWriter.new(@single_game_file.path).load

    assert_equal 3, GameState.read.length
    assert_equal "<world>", GameState.read[0][1]
    assert_equal "Isgalamido", GameState.read[0][2]
    assert_equal "MOD_TRIGGER_HURT", GameState.read[0][3]
  end

  def test_a_new_game_is_generated_at_each_game_init
    GameStateWriter.new(@multiple_games_file.path).load

    assert_equal 3, GameState.read.length
    assert_equal GameState.read[1][0], GameState.read[2][0] #comparing generated game names
    refute_equal GameState.read[1][0], GameState.read[0][0]
  end
end
