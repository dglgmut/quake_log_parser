require 'minitest/autorun'
require 'game_state_mapper'

class GameStateMapperTest < Minitest::Test

  def setup
    GameState.instance.clear
    2.times { GameState.write("game-1", "<world>", "suicidal_player", "MOD_TRIGGER_HURT") }
    10.times { GameState.write("game-1", "top_ranked_killer", "average_player", "MOD_ROCKET") }
    6.times { GameState.write("game-2", "top_ranked_killer", "average_player", "MOD_ROCKET") }
    6.times { GameState.write("game-2", "average_player", "another_average_player", "MOD_ROCKET") }
    2.times { GameState.write("game-2", "<world>", "suicidal_player", "MOD_TRIGGER_HURT") }
    @game_state_mapper = GameStateMapper.new(GameState.read)
  end

  def test_list_games_names
    assert_equal ["game-1", "game-2"], @game_state_mapper.games_names
  end

  def test_list_the_killer_names_at_the_game
    assert_equal ["<world>", "top_ranked_killer"], @game_state_mapper.killers_at("game-1").uniq
    assert_equal ["top_ranked_killer", "average_player", "<world>"], @game_state_mapper.killers_at("game-2").uniq
  end

  def test_list_death_causes_at_each_game
    assert_equal ["MOD_ROCKET", "MOD_TRIGGER_HURT"], @game_state_mapper.death_causes_at("game-2").uniq
  end

  def test_list_and_count_and_order_by_ranking_the_death_causes_of_all_games
    expectation = {"MOD_ROCKET"=>10, "MOD_TRIGGER_HURT"=>2}
    assert_equal expectation, @game_state_mapper.ordered_counting_of_death_causes_at("game-1")
  end

  def test_count_total_kills_from_a_game
    assert_equal 12, @game_state_mapper.count_kills_at("game-1")
  end

  def test_group_players_from_a_game
    assert_equal ["suicidal_player", "top_ranked_killer", "average_player"], @game_state_mapper.group_players_at("game-1")
  end

  def test_group_players_and_kills_from_a_game
    expectation = {"suicidal_player" => -2,
                   "top_ranked_killer" => 10,
                   "average_player" => 0}
    assert_equal expectation, @game_state_mapper.group_players_and_kills_at("game-1")
  end

  def test_group_and_count_and_order_by_ranking_based_on_total_kills_per_player_regardless_of_the_game
    expectation = {"top_ranked_killer"=>16,
                   "average_player"=>6,
                   "another_average_player"=>0,
                   "suicidal_player"=>-4}
    assert_equal expectation, @game_state_mapper.players_ordered_by_kill_count
  end
end
