require 'minitest/autorun'
require 'report_decorator'

class ReportDecoratorTest < Minitest::Test

  def setup
    GameState.instance.clear
    2.times { GameState.write("game-1", "<world>", "suicidal_player", "MOD_TRIGGER_HURT") }
    10.times { GameState.write("game-1", "top_ranked_killer", "average_player", "MOD_ROCKET") }
    6.times { GameState.write("game-2", "top_ranked_killer", "average_player", "MOD_ROCKET") }
    6.times { GameState.write("game-2", "average_player", "another_average_player", "MOD_ROCKET") }
    2.times { GameState.write("game-2", "<world>", "suicidal_player", "MOD_TRIGGER_HURT") }
    @report_decorator = ReportDecorator.new(GameStateMapper.new(GameState.read))
  end

  def test_show_game_state_with_total_kills_listing_the_players_and_detailed_score
    expectation = {
      "game-1"=>{
        "total_kills"=>12,
        "players"=>["suicidal_player", "top_ranked_killer", "average_player"],
        "kills"=>{"top_ranked_killer"=>10, "average_player"=>0, "suicidal_player"=>-2},
        "death_causes"=>{"MOD_ROCKET"=>10, "MOD_TRIGGER_HURT"=>2}
      }
    }
    assert_equal expectation, JSON.parse(@report_decorator.details_for("game-1"))
  end

  def test_show_players_ranking
    expectation = {"top_ranked_killer"=>16,
                   "average_player"=>6,
                   "another_average_player"=>0,
                   "suicidal_player"=>-4}
    assert_equal expectation, JSON.parse(@report_decorator.top_rank)
  end
end

