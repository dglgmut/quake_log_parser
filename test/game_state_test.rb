require 'minitest/autorun'
require 'game_state'

class GameStateTest < Minitest::Test
  def setup
    GameState.instance.clear
  end

  def test_write_always_takes_four_strings
    assert_raises ArgumentError do
      GameState.write("game_1")
    end
  end

  def test_inserting_a_record_successfully
    assert GameState.write("game_1", "killer_sample_name", "killed_sample_name", "MOD_UNKNOWN")
  end

  def test_that_game_state_is_readable
    assert GameState.read
  end
end
