# Planning Code Implementation:
  To create a reliable file reader, the following design patterns can be
  used:
  - Singleton: To store all the counting we will use a mutable object `GameState` which should inherit from an array since it will behave as a crude database, storing in the game name, the killer, the killed and the mean of death.
  - Decorator: To display the data, we will extend the `GameStateMapper`, making sure it generates the report in the appropriated format. There should be a decorator called `lib/report_decorator.rb` which shows the overall data from the game (accounting for world deaths as negative score)

  Beside those patterns there should be the following classes:
  - `bin/quake_log_parser.rb` this is suposed to be the file reader and report printer
  - `lib/game_state_writer.rb` this class will be called from the entry point, `QuakeLogParser` to read the log file and fill the `GameState`, it will also ignore some lines and asign the correct game for each line read.
  - `lib/game_state_mapper.rb` makes queries at the `GameState`
  - `lib/report_generator.rb` which will generate a file, printing the data built at the report_decorators

  Everything should be implemented according to the patterns specified inside [https://guides.rubygems.org/patterns]RubyGems , this way the setup will be easier for anyone that wants to use this app.


## TODO/estimatives
- [x] Setup the basic structure for the project, so it can behave as a Gem (15min)
- [x] Create tests and implement the GameState singleton (30min)
- [x] Create tests and implement the GameStateWriter (30min)
- [x] Create tests and implement the GameStateMapper (30min)
- [x] Create tests and implement the decorators (60min)
- [x] Create integration tests for the entire application (15min)
- [ ] Create a readme.md file so people can execute the project (10min)


Observations:
The parser wont be able to tell when the person joined the game if he does not die or does not kill anyone. It won't be able to tell track a player name change either (I don't think the logs specify those actions)
